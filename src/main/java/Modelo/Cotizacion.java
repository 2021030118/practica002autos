/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;
/**
 *
 * @author Angel
 */
public class Cotizacion {
    private int numCotizacion;
    private String descripcion;
    private float precio;
    private float porcetajePagoInicial;
    public int plazo;
    
    //Constructor por omisión
    public Cotizacion() { }
    
    //Segundo constructor
    public Cotizacion(Cotizacion cotizacion){
        this.numCotizacion = cotizacion.numCotizacion;
        this.descripcion = cotizacion.descripcion;
        this.precio = cotizacion.precio;
        this.porcetajePagoInicial = cotizacion.porcetajePagoInicial;
        this.plazo = cotizacion.plazo;
    }
    
    //Tercer constructor
    public Cotizacion(int numCotizacion, String descripcion, float precio, float porcentajePagoInicial, int plazo){}
    
    
    //Métodos GETTERS y SETTERS
    public void setNumCotizacion(int numCotizacion){
        this.numCotizacion = numCotizacion;
    }
    
    public int getNumCotizacion(){
        return numCotizacion;
    }
    
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public float getPrecio(){
        return precio;
    }
    
    public void setPorcentajePagoInicial(float porcentajePagoInicial){
        this.porcetajePagoInicial = porcentajePagoInicial;
    }
    
    public float getPorcentajePagoInicial(){
        return porcetajePagoInicial;
    }
    
    public void setPlazo(int plazo){
        this.plazo = plazo;
    }
    
    public int getPlazo(){
        return plazo;
    }
    
    //Métodos Calculados
    
    //Método Pago inicial
    public float pagoInicial(){
        return precio * (porcetajePagoInicial / 100);
    }
    
     //Método Total a financiar
    public float totalFinanciar(){ 
        return precio - pagoInicial();
    }
    
     //Método Pago Mensual
    public float pagoMensual(){   
        return totalFinanciar() /  plazo;
    }
}