package Controlador;

import Modelo.Cotizacion;
import Vista.dlgCotizacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Angel
 */
public class Controlador implements ActionListener{
   private Cotizacion coti;
   private dlgCotizacion vista;
   
   public Controlador(Cotizacion coti, dlgCotizacion vista){
       this.coti = coti;
       this.vista = vista;
       
       //Hacer que los botones sean escuchados
       vista.btnCancelar.addActionListener(this);
       vista.btnCerrar.addActionListener(this);
       vista.btnGuardar.addActionListener(this);
       vista.btnLimpiar.addActionListener(this);
       vista.btnMostrar.addActionListener(this);
       vista.btnNuevo.addActionListener(this);
       
       vista.btnGuardar.setEnabled(false);
       vista.btnMostrar.setEnabled(false);
   }
    
   public void IniciarVista(){
        vista.setTitle("::    COTIZACIONES   ::");
        vista.setSize(650, 650);
        vista.setVisible(true);
   }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         if (e.getSource() == vista.btnLimpiar) {
            vista.txtDescripcion.setText("");
            vista.txtFinanciamento.setText("");
            vista.txtNumeroCotizacion.setText("");
            vista.txtPagoInicial.setText("");
            vista.txtPagoMensual.setText("");
           vista.cbPlazo.setSelectedItem("0");
            vista.txtPorcentaje.setText("");
            vista.txtPrecio.setText("");
            
        }
        else if(e.getSource()== vista.btnNuevo){
           vista.txtDescripcion.enable(true);
           vista.txtNumeroCotizacion.enable(true);
           vista.cbPlazo.setEnabled(true);
           vista.txtPorcentaje.enable(true);
           vista.txtPrecio.enable(true);
           
           vista.btnGuardar.setEnabled(true);
           vista.btnMostrar.setEnabled(true);
           vista.btnLimpiar.setEnabled(true);
           
        }
        else if( e.getSource() == vista.btnGuardar ){
           try{
               coti.setDescripcion(vista.txtDescripcion.getText());
               coti.setNumCotizacion(Integer.parseInt(vista.txtNumeroCotizacion.getText()));
               coti.setPlazo(Integer.parseInt(vista.cbPlazo.getSelectedItem().toString()));
               coti.setPorcentajePagoInicial(Float.parseFloat(vista.txtPorcentaje.getText()));
               coti.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
               JOptionPane.showMessageDialog(vista, "SE GUARDARÓN CORRECTAMENTE LOS DATOS");
               limpiar();
           } catch(NumberFormatException ex){
               JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
           } catch (Exception ex2){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
            } 
           
           vista.btnGuardar.setEnabled(false);
           vista.btnLimpiar.setEnabled(false);
        }
        else if(e.getSource()==vista.btnMostrar){
            vista.txtDescripcion.setText(String.valueOf(coti.getDescripcion()));
            vista.txtFinanciamento.setText(String.valueOf(coti.totalFinanciar()));
            vista.txtNumeroCotizacion.setText(String.valueOf(coti.getNumCotizacion()));
            vista.txtPagoInicial.setText(String.valueOf(coti.pagoInicial()));
            vista.txtPagoMensual.setText(String.valueOf(coti.pagoMensual()));
            vista.cbPlazo.setSelectedItem(String.valueOf(coti.plazo));
            vista.txtPorcentaje.setText(String.valueOf(coti.getPorcentajePagoInicial()));
            vista.txtPrecio.setText(String.valueOf(coti.getPrecio()));
            
           vista.txtDescripcion.enable(true);
           vista.txtNumeroCotizacion.enable(true);
           vista.cbPlazo.setEnabled(true);
           vista.txtPorcentaje.enable(true);
           vista.txtPrecio.enable(true);
        }
        else if(e.getSource()==vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", "Decide",JOptionPane.YES_NO_OPTION);
            
            if(option == JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
            
        }
        else if(e.getSource()==vista.btnCancelar){
            vista.txtDescripcion.setText("");
            vista.txtFinanciamento.setText("");
            vista.txtNumeroCotizacion.setText("");
            vista.txtPagoInicial.setText("");
            vista.txtPagoMensual.setText("");
            vista.cbPlazo.setSelectedItem("0");
            vista.txtPorcentaje.setText("");
            vista.txtPrecio.setText("");
            
            vista.txtDescripcion.setEnabled(false);
            vista.txtFinanciamento.setEnabled(false);
            vista.txtNumeroCotizacion.setEnabled(false);
            vista.txtPagoInicial.setEnabled(false);
            vista.txtPagoMensual.setEnabled(false);
            vista.cbPlazo.setEnabled(false);
            vista.txtPorcentaje.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            
            vista.btnGuardar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            
        }
    }
    
    public static void main(String[] args) {
        Cotizacion coti = new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(), true);
        
        Controlador contra = new Controlador(coti, vista);
        contra.IniciarVista();
    }
    
     private void limpiar() {
        //throw new UnsupportedOperationException("Not yet implemented");
        vista.txtDescripcion.setText("");
        vista.txtFinanciamento.setText("");
        vista.txtNumeroCotizacion.setText("");
        vista.txtPagoInicial.setText("");
        vista.txtPagoMensual.setText("");
        vista.cbPlazo.setSelectedItem("0");
        vista.txtPorcentaje.setText("");
        vista.txtPrecio.setText("");
    }
}
